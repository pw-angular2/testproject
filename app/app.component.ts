import { Component, Inject, OnInit } from '@angular/core';

import { TranslateService } from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html'
}) 
export class AppComponent {
    constructor (@Inject(TranslateService) translate:TranslateService) {
        translate.setDefaultLang('pl');
        translate.use('pl');
    }
}