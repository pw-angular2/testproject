export interface ClientInterface {
    id: number;
    name: string;
    street: string;
    city: string;
    zip: string;
    nip: number;
    pesel: number;
    bankAccount: number;
    vendor: boolean;
}