import { ClientInterface } from './client.interface';
import { CoreModel } from 'core/model';
import { Http } from '@angular/http';

export class ClientModel extends CoreModel implements ClientInterface {

    id: number;
    name: string;
    street: string;
    city: string;
    zip: string;
    nip: number;
    pesel: number;
    bankAccount: number;
    vendor: boolean;

    protected url:string = 'data/client/:id';
    
    private client;
    
    constructor(client?: ClientInterface) {
        
        super();
        
        if (client) {
            for (let key in client) {
                this[key] = client[key];
            }
        }
    }
    
    get address():string {
        return [this.street, this.zip + " " + this.city].join(", ");
    }
}