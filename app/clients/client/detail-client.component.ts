import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ClientModel } from './client.model';

@Component({
    moduleId: module.id,
    selector: 'pw-detail-client',
    templateUrl: 'detail-client.component.html'
})
export class DetailClientComponent implements OnInit {
    client:ClientModel = new ClientModel();
    
    constructor(
        @Inject(ActivatedRoute) private router:ActivatedRoute) {}
    
    ngOnInit() {
        this.router.params.forEach((params: Params) => {
            let id = +params['id'];
            console.log('model?',this.client.load);
            console.log(this.client.load(id));
        });
    }

    cancel() {
        this.client = new ClientModel();
    }
    
    save() {
        this.client.save();
    }
}