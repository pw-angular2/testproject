import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ClientsService } from './clients.service';
import { ClientModel } from './client/client.model';

@Component({
    moduleId: module.id,
    selector: 'pw-clients',
    templateUrl: 'clients.component.html',
})
export class ClientsComponent implements OnInit {
    
    search:string;
    searchObj:Object = {};
    selectedClient:ClientModel;
    clients:ClientModel[] = [];
    header:Object[] = [
        {key: 'id', show: true, name: 'ID'},
        {key: 'name', show: true, name: 'Nazwa'},
        {key: 'address', show: true, name: 'Adres'},
        {key: 'nip', show: true, name: 'NIP'},
        {key: 'pesel', show: true, name: 'PESEL'},
        {key: 'bankAccount', show: true, name: 'Konto bankowe'},
        {key: 'vendor', show: true, name: 'Dostawca'},
    ];
    
    constructor(@Inject(ClientsService) private clientsService:ClientsService,
                @Inject(Router) private router:Router) {}
    
    selectClient(data:ClientModel) {
        this.router.navigate(['client/', data.id]);
    }
    
    cancel() {
         this.selectedClient = null;
    }
    
    ngOnInit() {
        this.clientsService.load((data) => { 
            this.clients = this.clientsService.clients
        });
    }
    
    onChangeSearch(search: string) {
        let s = search ? search.split(':') : [];
        
        if (s.length >=2 ) this.searchObj[s[0]] = s[1];
        else this.searchObj = {name: search};
    }
    
    onSubmit() {
        this.onChangeSearch(this.search);
    }
}