import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';

import { CoreModule }   from 'core/core.module';

import { ClientsComponent }         from './clients.component';
import { ClientsService }           from './clients.service';
import { DetailClientComponent }    from './client/detail-client.component';

@NgModule({
    imports: [
        CoreModule,
        BrowserModule,
        RouterModule.forRoot([
            {
                path: 'clients',
                component: ClientsComponent,
                data: {
                    title: 'CLIENTS.TITLE'
                }
            },
            {
                path: 'client/:id',
                component: DetailClientComponent
            }
        ]),
        HttpModule,
        FormsModule
    ],
    declarations: [ 
        ClientsComponent, 
        DetailClientComponent 
    ],
    exports: [ 
        ClientsComponent, 
        DetailClientComponent 
    ],
    providers: [ ClientsService ]
})
export class ClientsModule {}