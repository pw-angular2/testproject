import { Injectable, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http';

import { ClientModel } from './client/client.model';

@Injectable()
export class ClientsService implements OnInit {
    
    clients:ClientModel[] = [];
    
    constructor(@Inject(Http) private http:Http) {}
    
    load(callback) {
        return this.http.get('https://rozliczenia-dpych.c9users.io/data/clients.json')
            .subscribe((response) => {
                let res = response.json();
                let clients = [];
                
                for(let client of res.clients) {
                    clients.push(new ClientModel(client));
                }
                
                this.clients = clients;
                
                if (typeof callback === 'function') callback(this.clients);
            });
    }
    
    ngOnInit() {
        if (this.clients.length === 0) this.load();
    }
}