import { Inject, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpModule, Http } from '@angular/http';
import { RouterModule }   from '@angular/router';

import { CoreModule } from '../core/core.module';
import { ClientsModule } from './clients/clients.module';
import { ClientsService } from './clients/clients.service';

import { ProductsModule } from './products/products.module';

import { AppComponent } from './app.component';

@NgModule({
    imports: [
        BrowserModule, 
        CoreModule, 
        ClientsModule,
        HttpModule,
        ProductsModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'clients',
                pathMatch: 'full'
            }
        ],{ useHash: true})
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [ClientsService]
})
export class AppModule {
    constructor (@Inject(ClientsService) private clientsService:ClientsService) {}
}

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);