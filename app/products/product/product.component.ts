import { Component, Inject } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'pw-product',
    templateUrl: 'product.component.html'
})
export class ProductComponent {} 