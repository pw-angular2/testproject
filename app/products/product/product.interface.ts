export interface ProductInterface {
    id: number;
    name: string;
    price: string;
    vat: string;
    status: string;
}