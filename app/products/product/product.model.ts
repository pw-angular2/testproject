import { CoreModel } from 'core/model';

import { ProductInterface } from './product.interface';

export class ProductModel extends CoreModel implements ProductInterface {

    id: number;
    name: string;
    price: string;
    vat: string;
    status: string;

    private product;
    
    constructor(product?: ProductInterface) {
        if (product) {
            for (let key in product) {
                this[key] = product[key];
            }
        }
    }
}