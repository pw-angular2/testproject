import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'pw-products',
    templateUrl: 'products.component.html',
})
export class ProductsComponent {
    products = [];
    header:Object[] = [
        {key: "id", show: true, name: "DEFAULT.ID"},
        {key: "name", show: true, name: "PRODUCTS.NAME"},
        {key: "price", show: true, name: "PRODUCTS.PRICE"},
        {key: "vat", show: true, name: "PRODUCTS.VAT"},
        {key: "active", show: true, name: "PRODUCTS.ACTIVE"}
    ];
    
    selectProduct() {}
}