import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';

import { CoreModule }   from 'core/core.module';

import { ProductsComponent }    from './products.component';
import { ProductComponent }     from './product/product.component';
import { ProductModel }         from './product/product.model';

@NgModule({
    imports: [
        CoreModule,
        BrowserModule,
        RouterModule.forRoot([
            {
                path: 'products',
                component: ProductsComponent,
                data: {
                    title: 'PRODUCTS.TITLE'
                }
            },
            {
                path: 'product/:id',
                component: ProductComponent
            }
        ]),
        HttpModule,
        FormsModule
    ],
    declarations: [ 
        ProductsComponent,
        ProductComponent
    ],
    exports: [ 
        ProductsComponent,
        ProductComponent
    ],
    providers: []
})
export class ProductsModule {}