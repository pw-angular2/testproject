import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { RouterModule }   from '@angular/router';
import { HttpModule }    from '@angular/http';

import { TranslateModule } from 'ng2-translate';

import { CoreNavComponent } from './nav/nav.component';
import { TableComponent } from './table/table.component';
import { CoreFilterPipe } from './filter.pipe';
import { CoreSortByPipe } from './sortby.pipe';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        HttpModule,
        TranslateModule.forRoot()
    ],
    declarations: [
        CoreFilterPipe,
        CoreSortByPipe,
        CoreNavComponent,
        TableComponent
    ],
    exports: [
        CoreFilterPipe,
        CoreSortByPipe,
        CoreNavComponent,
        TableComponent,
        TranslateModule
    ]
})
export class CoreModule {}