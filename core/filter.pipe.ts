import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pwfilter',
    pure: false
})
@Injectable()
export class CoreFilterPipe implements PipeTransform {
    transform(items: any[], args: any[]): any {
        let ret = [];
        
        if (args && Object.keys(args).length > 0) {
            
            for (let item of items) {
                for (let key in args) {
                    let rq = new RegExp(args[key], 'gi');
                    if (rq.test(item[key])) ret.push(item);
                }
            }
            
        } else {
            ret = items;
        }
        
        return ret;
    }
}