import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CoreHttpService {
    url: string;
    
    constructor(public http:Http) {}
    
    init(url) {
        this.url = url;
        
        return this;
    }
    
    get() {
        return this.http.get(this.url);
    }
}