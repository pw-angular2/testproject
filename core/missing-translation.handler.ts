import { MissingTranslationHandler } from 'ng2-translate';

export class CoreMissingTranslationHandler implements MissingTranslationHandler {
    handle(key: string) {
        return key;
    }
}