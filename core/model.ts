import { Inject, ReflectiveInjector, Injectable } from '@angular/core';
import { CoreHttpService } from './http.service';
import { Http } from '@angular/http';

export class CoreModel {
    id: number;
    
    protected url:string;
    
    constructor(@Inject(CoreHttpService) http:CoreHttpService) {
        console.log(http);
    }
    
    create() {}
    
    load(id: number) {
        
        if (this.url && id) {
            //return this.http.get(this.url + id + '.json');
                // .toPromise()
                // .then((response) => response.json());
        }
    }
    
    remove() {}
    
    save() {
        if (!this.id) {
            this.create();
        } else {
            
        }
    }
}