import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'pw-nav',
    templateUrl: 'nav.component.html'
})
export class CoreNavComponent implements OnInit {
    navs:Object[] = [];
    
    constructor(@Inject(Router) private router:Router) {}
    
    ngOnInit() {
        for (let nav of this.router.config) {
            console.log(nav.data);
            if (nav.data && nav.data.title) this.navs.push(nav);
        }
    }
}