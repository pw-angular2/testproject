import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pwsortBy',
    pure: false
})
@Injectable()
export class CoreSortByPipe implements PipeTransform {
    transform(items: any[], args: any[]): any {
        array.sort((a: any, b: any) => {
            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            } else {
                return 0;
            }
        });
        return array;
    }
}