export interface TableHeaderInterface {
    key: string;
    name: string;
    show: boolean;
    width?: number;
    
}