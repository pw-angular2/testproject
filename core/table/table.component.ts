import { Component, EventEmitter, OnChanges, OnInit, Output, Input } from '@angular/core';

import { TableHeaderInterface } from './table-header.interface';

@Component({
    moduleId: module.id,
    selector: 'pw-table',
    templateUrl: 'table.component.html'
})
export class TableComponent implements OnInit {
    @Input() data:any[];
    @Input() header?:TableHeaderInterface[];
    @Output() clickRow?:EventEmitter = new EventEmitter();
    
    lp:TableHeaderInterface = {
        key: "lp",
        show: true,
        name: "Lp."
    };
    
    clickOnRow(data) {
        if (this.clickRow) {
            this.clickRow.emit(data);
        }
    }
    
    ngOnInit() {
        let header = [];
        
        if (this.header) {
            for (let col in this.header) {
                if (this.header[col].key === 'lp') {
                    this.lp = this.header[col];
                } else {
                    header.push(this.header[col]);
                }
            }
        }
        
        if ((!this.header || this.header.length === 0) && this.data && this.data[0]) {
            for (let key in this.data[0]) {
                if (this.data[0].hasOwnProperty(key)) {
                    header.push({ key: key, name: key, show: true});
                }
            }
        }
        
        this.header = header;
    }
    
    ngOnChanges() {
        this.ngOnInit();
    }
}