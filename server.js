"use strict";
var connect = require("connect");
var serveStatic = require("serve-static");

var s = serveStatic(__dirname);

connect().use(s).listen(process.env.PORT, function () {
    console.log("Server running on " + process.env.PORT + "...");
});